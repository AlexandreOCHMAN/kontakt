# Guide du développeur Kontakt

## Pour lancer l'infrastructure
### Prérequis
    cd infrastructures
    cp .env.dist .env

Récupérer l'IP du container et copier dans le fichier host local

    sudo echo $(docker network inspect bridge | grep Gateway | grep -o -E '([0-9]{1,3}\.){3}[0-9]{1,3}') "kontakt.local" >> /etc/hosts
Si cela ne fonctionne pas, dans etc/hosts mettre 127.0.0.1 pour kontakt.local

    127.0.0.1 kontakt.local

### Containers
Compiler les containers

    docker-compose build

Lancer les containers

    docker-compose up

Accéder à un container

    docker exec -it nomcontainer bash


## remove a docker volume

Lister les volumes

    docker volume ls
    
Supprimer un volume

    docker volume rm NomDuVolume
<?php
// src/Controller/ContactController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;

class ContactController extends AbstractController
{
    public function contact(UserInterface $user)
    {
        $em = $this->getDoctrine()->getManager(); // ...or getEntityManager() prior to Symfony 2.1
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT email FROM user as u LEFT JOIN contact_user as c ON u.id=c.id_friend WHERE c.id_user = :id");
        $statement->bindValue('id', $user->getId());
        $statement->execute();
        $results = $statement->fetchAll();
        return $this->render('kontakt-list.html.twig', array('data' => $results));
    }
}
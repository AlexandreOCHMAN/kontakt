<?php

namespace App\Controller;

use App\Entity\ContactUser;
use App\Form\ContactUserType;
use App\Repository\ContactUserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/contact/user")
 */
class ContactUserController extends AbstractController
{
    /**
     * @Route("/", name="contact_user_index", methods={"GET"})
     */
    public function index(ContactUserRepository $contactUserRepository): Response
    {
        return $this->render('contact_user/index.html.twig', [
            'contact_users' => $contactUserRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="contact_user_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $contactUser = new ContactUser();
        $form = $this->createForm(ContactUserType::class, $contactUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contactUser);
            $entityManager->flush();

            return $this->redirectToRoute('contact_user_index');
        }

        return $this->render('contact_user/new.html.twig', [
            'contact_user' => $contactUser,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="contact_user_show", methods={"GET"})
     */
    public function show(ContactUser $contactUser): Response
    {
        return $this->render('contact_user/show.html.twig', [
            'contact_user' => $contactUser,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="contact_user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ContactUser $contactUser): Response
    {
        $form = $this->createForm(ContactUserType::class, $contactUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('contact_user_index', [
                'id' => $contactUser->getId(),
            ]);
        }

        return $this->render('contact_user/edit.html.twig', [
            'contact_user' => $contactUser,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="contact_user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ContactUser $contactUser): Response
    {
        if ($this->isCsrfTokenValid('delete'.$contactUser->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($contactUser);
            $entityManager->flush();
        }

        return $this->redirectToRoute('contact_user_index');
    }
}

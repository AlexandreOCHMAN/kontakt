<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiController extends AbstractController
{
    /**
     * @Route("/api", name="api")
     */
    public function index(UserInterface $user)
    {
        $em = $this->getDoctrine()->getManager(); // ...or getEntityManager() prior to Symfony 2.1
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT email FROM user as u LEFT JOIN contact_user as c ON u.id=c.id_friend WHERE c.id_user = :id");
        $statement->bindValue('id', $user->getId());
        $statement->execute();
        $results = $statement->fetchAll();

        $response = new JsonResponse();
        $response->setData($results);

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
